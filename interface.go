package task

import "bitbucket.org/to-increase/go-task/event"

type DispatchPropertyKey string

type DispatchPropertyValue struct {
	Key     string
	Default interface{}
}

const TimeoutKey DispatchPropertyKey = DispatchPropertyKey("timeout")
const timeoutKeyDefault int = 500
const MaxConcurrentRequestsKey DispatchPropertyKey = DispatchPropertyKey("maxConcurrentRequests")
const maxConcurrentRequestsDefault int = 10
const ErrorPercentThresholdKey DispatchPropertyKey = DispatchPropertyKey("errorPercentThreshold")
const errorPercentThresholdDefault int = 25
const SleepWindowKey DispatchPropertyKey = DispatchPropertyKey("sleepWindow")
const sleepWindowDefault int = 5000 //milliseconds

type MessageBroker interface {
	Event(source Dispatcher, et event.EventType, payload interface{})
}

type Work interface {
	Do(MessageBroker) func() error
	Fallback(MessageBroker) func(error) error
}

type Dispatcher interface {
	// The name of this dispatcher
	Name() string
	// Update the configuration settings of this dispatcher
	Update()
	// Execute work immediately. The work itself can fail if there is no free capacity or a timeout occurs.
	Execute(Work) <-chan error
	// Queue work. When the queue is full, return an error. Besides that, a read-only channel
	// is returned providing the error channel of the eventually executed job.
	Queue(Work) (error, <-chan error)
	// Queue work. When the queue is full, block untill the work is actually placed on the queue.
	BlockingQueue(Work) <-chan error
}

func Property(key string, default_value interface{}) DispatchPropertyValue {
	return DispatchPropertyValue{Key: key, Default: default_value}
}

type DispatcherFactory interface {
	NewDispatcher(name string, properties map[DispatchPropertyKey]DispatchPropertyValue) Dispatcher
}
