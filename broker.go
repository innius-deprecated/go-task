package task

import (
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-logger"
	"bitbucket.org/to-increase/go-task/event"
)

type noopBroker struct {
}

func NewNoOpBroker() MessageBroker {
	return &noopBroker{}
}

func (noop *noopBroker) Event(source Dispatcher, e event.EventType, p interface{}) {
}

type logMessageBroker struct {
	ctx context.TaskContext
	log *logger.Logger
}

func NewLogMessageBroker(c context.TaskContext) MessageBroker {
	return &logMessageBroker{
		ctx: c,
		log: c.Logger(),
	}
}

func (lmb *logMessageBroker) Event(source Dispatcher, e event.EventType, p interface{}) {
	lmb.log.Debugf("%v: %v - %v", source.Name(), e, p)
}
