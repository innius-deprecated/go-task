package task

import (
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/types"
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-task/event"
	"bitbucket.org/to-increase/go-tickets"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/pkg/errors"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type dispatcher struct {
	broker                MessageBroker
	name                  string
	queue                 chan *queued_work
	circuit               *hystrix.CircuitBreaker
	tickets               ticket.TicketPool
	mutex                 *sync.RWMutex
	timeout               types.DynamicIntValue
	maxConcurrentRequests types.DynamicIntValue
	errorPercentThreshold types.DynamicIntValue
	sleepWindow           types.DynamicIntValue
	openedOrLastTestTime  int64
}

type dispatcher_config_factory struct {
	context context.TaskContext
	config  config.Configuration
	broker  MessageBroker
	prefix  string
}

type queued_work struct {
	e chan error
	w Work
}

const queued_work_limit int = 100
const max_queued_work_capacity int = 80 // Execute queued tasks at a maximum of 80 % pool capacity
const queue_schedule_interval time.Duration = 100 * time.Millisecond

func NewDispatcherFactory(ctx context.TaskContext, prefix string, c config.Configuration, broker MessageBroker) DispatcherFactory {
	d := &dispatcher_config_factory{
		context: ctx,
		config:  c,
		broker:  broker,
		prefix:  prefix,
	}

	if ctx == nil {
		d.context = context.NewTaskCtx()
	}

	if c == nil {
		d.config = config.NewConfigurator()
	}

	if broker == nil {
		d.broker = NewNoOpBroker()
	}

	return d
}

func (t *dispatcher_config_factory) pfx(val string) string {
	return t.prefix + "/" + val
}

func (t *dispatcher_config_factory) NewDispatcher(name string, properties map[DispatchPropertyKey]DispatchPropertyValue) Dispatcher {
	tc := &dispatcher{
		name:    name,
		broker:  t.broker,
		queue:   make(chan *queued_work, queued_work_limit),
		tickets: ticket.NewTicketPool(1), // gets updated via Update, this is just an initial value
		mutex:   &sync.RWMutex{},
	}
	for k, v := range properties {
		switch k {
		case TimeoutKey:
			tc.timeout = t.config.GetInt(t.pfx(v.Key), as_int(v.Default, timeoutKeyDefault))
			tc.timeout.OnChange(func(p types.Path, value int) {
				tc.Update()
			})
		case MaxConcurrentRequestsKey:
			tc.maxConcurrentRequests = t.config.GetInt(t.pfx(v.Key), as_int(v.Default, maxConcurrentRequestsDefault))
			tc.maxConcurrentRequests.OnChange(func(p types.Path, value int) {
				tc.Update()
			})
		case ErrorPercentThresholdKey:
			tc.errorPercentThreshold = t.config.GetInt(t.pfx(v.Key), as_int(v.Default, errorPercentThresholdDefault))
			tc.errorPercentThreshold.OnChange(func(p types.Path, value int) {
				tc.Update()
			})
		case SleepWindowKey:
			tc.sleepWindow = t.config.GetInt(t.pfx(v.Key), as_int(v.Default, sleepWindowDefault))
			tc.sleepWindow.OnChange(func(p types.Path, value int) {
				tc.Update()
			})
		}
	}

	tc.Update()

	circuit, _, _ := hystrix.GetCircuit(name)
	tc.circuit = circuit

	// Separate go routine to deal with queued work.
	go func() {
		for {
			if tc.tickets.PercentageUsed() <= max_queued_work_capacity && (!circuit.IsOpen() || tc.sleepComplete()) {
				ticket := tc.tickets.GetTicket()
				// If we have a ticket, it's not busy, and we schedule as much as we can. If it failed,
				// we sleep for a while and try again. Note: we reserve a maximum of one ticket here if
				// there is no job on the queue. In that case we ought to return it.
				if ticket != nil {
					select {
					case qw := <-tc.queue:
						retry_chan := make(chan error, 1)
						go tc.execute(ticket, retry_chan, qw.w)
						go func() {
							e, ok := <-retry_chan
							if !ok { // The error channel is closed.
								close(qw.e)
							} else if e == nil { // not closed, but not an err either.
								qw.e <- e
								close(qw.e)
							} else if shouldRetry(e) { //really special case: retry on hystrix errors
								tc.queue <- qw
							} else { // the other errors should also be propagated.
								qw.e <- e
								close(qw.e)
							}
						}()
						// Do as much work as we can while there are tickets left.
						continue
					default:
						tc.tickets.Return(ticket)
					}
				}
			}
			// Sleep for a while if there is no work.
			time.Sleep(queue_schedule_interval)
		}
	}()

	return tc
}

func shouldRetry(e error) bool {
	if strings.Contains(e.Error(), "hystrix: ") {
		if strings.Contains(e.Error(), "max concurrency") {
			return true
		}
		if strings.Contains(e.Error(), "circuit open") {
			return true
		}
	}
	return false
}

func optionalDynamicIntAsValue(d types.DynamicIntValue, def int) int {
	if d == nil {
		return def
	} else {
		return d.Value()
	}
}

func as_int(v interface{}, def int) int {
	switch v := v.(type) {
	case int:
		return v
	case string:
		_v, e := strconv.Atoi(v)
		if e != nil {
			return def
		} else {
			return _v
		}
	default:
		return def
	}
}

func (t *dispatcher) Name() string {
	return t.name
}

func (t *dispatcher) Update() {
	hystrix.ConfigureCommand(t.name, hystrix.CommandConfig{
		Timeout:               optionalDynamicIntAsValue(t.timeout, timeoutKeyDefault),
		MaxConcurrentRequests: optionalDynamicIntAsValue(t.maxConcurrentRequests, maxConcurrentRequestsDefault),
		ErrorPercentThreshold: optionalDynamicIntAsValue(t.errorPercentThreshold, errorPercentThresholdDefault),
		SleepWindow:           optionalDynamicIntAsValue(t.sleepWindow, sleepWindowDefault),
	})
	t.tickets.Resize(optionalDynamicIntAsValue(t.maxConcurrentRequests, maxConcurrentRequestsDefault))
}

// This func determines whether we should retry a single request again. The circuit can be open,
// in which case we should try a request every now and then.
func (t *dispatcher) sleepComplete() bool {
	now := time.Now().UnixNano()
	diff := int64(optionalDynamicIntAsValue(t.sleepWindow, sleepWindowDefault) * 1000 * 1000) // nanos

	openedOrLastTestedTime := atomic.LoadInt64(&t.openedOrLastTestTime)
	if now > openedOrLastTestedTime+diff {
		swapped := atomic.CompareAndSwapInt64(&t.openedOrLastTestTime, openedOrLastTestedTime, now)
		return swapped
	}
	return false
}

func (t *dispatcher) Execute(w Work) <-chan error {
	err := make(chan error, 1)
	ticket := t.tickets.GetTicket()
	if ticket != nil {
		t.execute(ticket, err, w)
	} else {
		err <- errors.New("Can't execute work -- no more tickets available.")
	}
	return err
}

func (t *dispatcher) execute(tick ticket.Ticket, err chan error, w Work) {
	t.broker.Event(t, event.Schedule, nil)

	// Ok, a job's error channel will not be closed by hystrix. We therefore wrap jobs in a handler that notifies us of completion.
	done := make(chan struct{}, 1)
	wrapperPrimary := func() error {
		t.broker.Event(t, event.Start, nil)
		defer func() {
			if r := recover(); r != nil {
				err <- errors.Errorf("Primary failed: %v", r)
			}
		}()
		err := w.Do(t.broker)()
		if err != nil {
			return err
		}

		done <- struct{}{}
		return nil
	}

	wrapperFallback := func(e error) error {
		t.broker.Event(t, event.StartFallback, nil)
		defer func() {
			if r := recover(); r != nil {
				err <- errors.Errorf("Fallback wrapper failed: %v", r)
			}
		}()
		err := w.Fallback(t.broker)(e)
		if err != nil {
			return err
		}

		done <- struct{}{}
		return nil
	}

	// Now, actually do execute the job.
	h_err := hystrix.Go(t.name, wrapperPrimary, wrapperFallback)
	// And properly manage signaling and failures.
	go func() {
		select {
		case <-done:
			t.broker.Event(t, event.Complete, nil)
		case e := <-h_err:
			if e != nil {
				t.broker.Event(t, event.Error, e)
			} else {
				t.broker.Event(t, event.Complete, nil)
			}
			err <- e
		}
		t.tickets.Return(tick)
		close(err)
	}()
}

func (t *dispatcher) Queue(w Work) (error, <-chan error) {
	qw := &queued_work{
		w: w,
		e: make(chan error, 1),
	}

	// Try to write the work to the queue:
	select {
	case t.queue <- qw:
		return nil, qw.e
	default:
		close(qw.e)
		return errors.New("Maximum queued job limit reached."), qw.e
	}
}

func (t *dispatcher) BlockingQueue(w Work) <-chan error {
	qw := &queued_work{
		w: w,
		e: make(chan error, 1),
	}

	t.queue <- qw
	return qw.e
}
