package task_test

import (
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/sources"
	"bitbucket.org/to-increase/go-context"
	mb "bitbucket.org/to-increase/go-mountebank"
	"bitbucket.org/to-increase/go-sdk/services/authorization"
	"bitbucket.org/to-increase/go-sdk/tests/integration/testutils"
	"bitbucket.org/to-increase/go-task"
	"bitbucket.org/to-increase/go-trn"
	"fmt"
	"github.com/stretchr/testify/assert"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestDispatchedPanic(t *testing.T) {
	c := config.NewConfigurator()

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestJobWithPanic", map[task.DispatchPropertyKey]task.DispatchPropertyValue{})

	hit := false

	w := task.NewWork(func(d task.MessageBroker) func() error {
		return func() error {
			panic("Panicky-panic")
		}
	}, func(d task.MessageBroker) func(error) error {
		return func(e error) error {
			hit = true
			return nil
		}
	})

	err := f.Execute(w)

	select {
	case e := <-err:
		assert.NotNil(t, e)
	case <-time.After(500 * time.Millisecond):
		assert.False(t, hit, "The fallback should NOT be invoked after a panic.")
		assert.Fail(t, "The panic should have returned an error.")
	}
}

func TestDispatchedFromFallbackPanic(t *testing.T) {
	c := config.NewConfigurator()

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestJobWithPanic", map[task.DispatchPropertyKey]task.DispatchPropertyValue{})

	w := task.NewWork(func(d task.MessageBroker) func() error {
		return func() error {
			return fmt.Errorf("Pirates are detected!")
		}
	}, func(d task.MessageBroker) func(error) error {
		return func(e error) error {
			panic("Boom :: true pirates arrived!")
		}
	})

	err := f.Execute(w)

	select {
	case e := <-err:
		assert.NotNil(t, e)
	case <-time.After(500 * time.Millisecond):
		assert.Fail(t, "Panic should make the work throw an error.")
	}
}

func TestConcurrentJobExecutionLimitExtraJobsShouldFail(t *testing.T) {
	m := sources.NewMemorySource()
	m.Set("timeout", 100)
	m.Set("conc", 10)
	c := config.NewConfigurator(m)

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestConcurrentJobExecutionLimitExtraJobsShouldFail", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey:               task.Property("timeout", 5),
		task.MaxConcurrentRequestsKey: task.Property("conc", 5),
	})

	work := make([]task.Work, 0, 0)
	success := 0
	failed := 0
	lock := sync.Mutex{}

	for i := 0; i < 15; i++ {
		w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
			return func() error {
				lock.Lock()
				success = success + 1
				lock.Unlock()
				time.Sleep(75 * time.Millisecond)
				return nil
			}
		})
		work = append(work, w)
	}

	for _, w := range work {
		err := f.Execute(w)
		go func() {
			e := <-err
			if e != nil {
				lock.Lock()
				failed = failed + 1
				lock.Unlock()
			}
		}()
	}

	select {
	case <-time.After(500 * time.Millisecond):
		assert.Equal(t, 10, success)
		assert.Equal(t, 5, failed)
	}
}

func TestConcurrentJobExecutionLimitExtraQueuedJobsShouldSucceed(t *testing.T) {
	m := sources.NewMemorySource()
	m.Set("timeout", 100)
	m.Set("conc", 10)
	c := config.NewConfigurator(m)

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestConcurrentJobExecutionLimitExtraQueuedJobsShouldSucceed", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey:               task.Property("timeout", 5),
		task.MaxConcurrentRequestsKey: task.Property("conc", 5),
	})

	work := make([]task.Work, 0, 0)
	success := 0
	failed := 0
	lock := sync.Mutex{}

	for i := 0; i < 15; i++ {
		w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
			return func() error {
				lock.Lock()
				success = success + 1
				lock.Unlock()
				time.Sleep(75 * time.Millisecond)
				return nil
			}
		})
		work = append(work, w)
	}

	for _, w := range work {
		e, err := f.Queue(w)
		assert.Nil(t, e)
		go func() {
			e := <-err
			if e != nil {
				lock.Lock()
				failed = failed + 1
				lock.Unlock()
			}
		}()
	}

	select {
	case <-time.After(500 * time.Millisecond):
		assert.Equal(t, 15, success)
		assert.Equal(t, 0, failed)
	}
}

func TestQueuedJobsShouldSucceedEvenAsNormalWorkIsQueued(t *testing.T) {
	m := sources.NewMemorySource()
	m.Set("timeout", 80)
	m.Set("conc", 10)
	c := config.NewConfigurator(m)

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestQueuedJobsShouldSucceedEvenAsNormalWorkIsQueued", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey:               task.Property("timeout", 5),
		task.MaxConcurrentRequestsKey: task.Property("conc", 5),
	})

	work := make([]task.Work, 0, 0)
	success := 0
	failed := 0
	lock := sync.Mutex{}

	for i := 0; i < 15; i++ {
		w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
			return func() error {
				lock.Lock()
				success = success + 1
				lock.Unlock()
				time.Sleep(75 * time.Millisecond)
				return nil
			}
		})
		work = append(work, w)
	}

	cnt := 0
	for _, w := range work {
		if cnt < 10 {
			err := f.Execute(w)
			go func() {
				e := <-err
				if e != nil {
					lock.Lock()
					failed = failed + 1
					lock.Unlock()
				}
			}()
		} else {
			e, err := f.Queue(w)
			assert.Nil(t, e)
			go func() {
				e := <-err
				if e != nil {
					lock.Lock()
					failed = failed + 1
					lock.Unlock()
				}
			}()
		}
		cnt++
	}

	select {
	case <-time.After(500 * time.Millisecond):
		assert.Equal(t, 15, success)
		assert.Equal(t, 0, failed)
	}
}

func TestOverflowQueue(t *testing.T) {
	c := config.NewConfigurator()

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestOverflowQueue", map[task.DispatchPropertyKey]task.DispatchPropertyValue{})

	work := make([]task.Work, 0, 0)

	for i := 0; i < 150; i++ {
		w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
			return func() error {
				time.Sleep(5 * time.Second)
				return nil
			}
		})
		work = append(work, w)
	}

	ok := 0
	failed := 0
	for _, w := range work {
		e, _ := f.Queue(w)
		if e == nil {
			ok = ok + 1
		} else {
			failed = failed + 1
		}
	}
	// Depending on the scheduling, between 100 and 110 jobs will be ok, and 40/50 will be failed.
	assert.True(t, ok >= 100)
	assert.True(t, ok <= 110)
	assert.True(t, failed >= 40)
	assert.True(t, failed <= 50)
}

// note: this test failed because a really really small portion of the jobs failed when it got a ticket from the dispatcher,
// but required a hystrix ticket as well. The latter was then not yet returned tot the hystrix pool, resulting in a circuit error.
// This test verifies that jobs failed with either a hystrix circuit open error or a hystrix max concurrency error would be retried.
func TestOverflowBlockingQueueNotice(t *testing.T) {
	fmt.Printf("\n\n\tThe test 'TestOverflowBlockingQueue' fails on shippable. It is disabled by default, but it is an important testcase.\n\n\n")
}
func TestOverflowBlockingQueue(t *testing.T) {
	return // thank you shippable
	c := config.NewConfigurator()

	jobsize := 25000

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestOverflowBlockingQueue", map[task.DispatchPropertyKey]task.DispatchPropertyValue{})

	work := make([]task.Work, 0, 0)

	var wg sync.WaitGroup
	wg.Add(jobsize)

	var count int32 = 0
	for i := 0; i < jobsize; i++ {
		w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
			return func() error {
				atomic.AddInt32(&count, 1)
				return nil
			}
		})
		work = append(work, w)
	}

	var ok int32 = 0
	var failed int32 = 0
	for _, w := range work {
		go func() {
			errchan := f.BlockingQueue(w)
			e := <-errchan
			if e == nil {
				atomic.AddInt32(&ok, 1)
			} else {
				fmt.Printf("Error: %v\n", e)
				atomic.AddInt32(&failed, 1)
			}
			wg.Done()
		}()
	}

	ticker := time.NewTicker(time.Second * 3)
	go func() {
		for {
			<-ticker.C
			fmt.Printf("Count: %v, ok: %v, failed: %v\n", count, ok, failed)
		}
	}()

	wg.Wait()
	// Depending on the scheduling, between 100 and 110 jobs will be ok, and 40/50 will be failed.
	assert.Equal(t, int32(jobsize), count)
	assert.Equal(t, int32(jobsize), ok)
	assert.Equal(t, int32(0), failed)
}

func TestBreakingDispatcherNotice(t *testing.T) {
	fmt.Printf("\n\n\tThe test 'TestBreakingDispatcherCircuit' fails on shippable. It is disabled by default, but it is an important testcase.\n\n\n")
}

//This test a scenario where the dispatcher hystrix circuit would never do its work once it was closed. Returns immediately because it doesn't run on Shippable.
func TestBreakingDispatcherCircuit(t *testing.T) {
	return //thanks again shippable
	c := config.NewConfigurator()

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("TestBreakingDispatcher", map[task.DispatchPropertyKey]task.DispatchPropertyValue{})

	r := `
    {
        "subject" :  "trn:plantage:company:plantage",
        "resource" : "trn:plantage:machine:1234",
        "action" : "get",
        "effect" : 1
    }
    `
	ctx := context.NewCtx()
	in := &authorization.AuthorizeInput{
		Action:   "get",
		Subject:  trn.New("plantage", trn.Company, "plantage"),
		Resource: trn.New("plantage", trn.Machine, "1234"),
	}

	imp1, err := mb.DefaultImposter("POST", "/authorize", r, 500, 0)

	assert.Nil(t, err)

	badclient := authorization.New(testutils.NewConfig().WithEndpoint(fmt.Sprintf("http://localhost:%v", imp1.Port)))

	badwork := 30 //slightly over the max concurrency limit
	goodwork := 100

	badWork := make([]task.Work, 0, 0)
	goodWork := make([]task.Work, 0, 0)

	var wg sync.WaitGroup
	wg.Add(badwork)

	for i := 0; i < badwork; i++ {
		w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
			return func() error {
				respChan, errChan := badclient.Authorize(ctx, in)
				select {
				case <-respChan:
					return nil
				case e := <-errChan:
					return e
				}
			}
		})
		badWork = append(badWork, w)
	}

	var ok int32 = 0
	var failed int32 = 0
	for _, w := range badWork {
		go func() {
			errchan := f.BlockingQueue(w)
			e := <-errchan
			if e == nil {
				atomic.AddInt32(&ok, 1)
			} else {
				atomic.AddInt32(&failed, 1)
			}
			wg.Done()
		}()
	}
	wg.Add(goodwork)

	imp2, err := mb.DefaultImposter("POST", "/authorize", r, 200, 0)

	goodclient := authorization.New(testutils.NewConfig().WithEndpoint(fmt.Sprintf("http://localhost:%v", imp2.Port)))

	for i := 0; i < goodwork; i++ {
		w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
			return func() error {
				respChan, errChan := goodclient.Authorize(ctx, in)
				select {
				case <-respChan:
					return nil
				case e := <-errChan:
					return e
				}
			}
		})
		goodWork = append(goodWork, w)
	}

	for _, w := range goodWork {
		go func() {
			errchan := f.BlockingQueue(w)
			e := <-errchan
			if e == nil {
				atomic.AddInt32(&ok, 1)
			} else {
				atomic.AddInt32(&failed, 1)
			}
			wg.Done()
		}()
	}

	wg.Wait()

	assert.Equal(t, int32(goodwork), ok)
	assert.Equal(t, int32(badwork), failed)
}
