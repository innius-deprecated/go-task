package task

type work struct {
	do func(m MessageBroker) func() error
	fb func(m MessageBroker) func(error) error
}

func (w *work) Do(mb MessageBroker) func() error {
	return w.do(mb)
}

func (w *work) Fallback(mb MessageBroker) func(error) error {
	return w.fb(mb)
}

func NewWork(f func(m MessageBroker) func() error, g func(MessageBroker) func(error) error) Work {
	return &work{
		do: f,
		fb: g,
	}
}

func NewWorkWithoutFallback(f func(m MessageBroker) func() error) Work {
	return NewWork(f, func(m MessageBroker) func(error) error {
		return func(e error) error {
			return e
		}
	})
}
