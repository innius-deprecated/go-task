package task_test

import (
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/sources"
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-task"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestErrorEventOnChannel(t *testing.T) {
	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", nil, task.NewNoOpBroker())

	f := tf.NewDispatcher("foo", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey: task.Property("/foo/bar/mytimeout", 5),
	})

	w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
		return func() error {
			return errors.New("abc")
		}
	})

	err := f.Execute(w)

	errorred := false
	select {
	case <-err:
		errorred = true
	case <-time.After(50 * time.Millisecond):
		assert.True(t, errorred)
	}
}

func TestErrorChannelClose(t *testing.T) {
	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", nil, task.NewNoOpBroker())

	f := tf.NewDispatcher("foo", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey: task.Property("/foo/bar/mytimeout", 5),
	})
	w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
		return func() error {
			return nil
		}
	})

	err := f.Execute(w)

	select {
	case <-err:
	case <-time.After(500 * time.Millisecond):
		assert.Fail(t, "Channel should have been closed")
	}
}

func TestErrorChannelCloseWithSuccessFallback(t *testing.T) {
	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", nil, task.NewNoOpBroker())

	f := tf.NewDispatcher("foo", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey: task.Property("/foo/bar/mytimeout", 5),
	})

	w := task.NewWork(func(d task.MessageBroker) func() error {
		return func() error {
			return errors.New("abc")
		}
	}, func(d task.MessageBroker) func(error) error {
		return func(e error) error {
			return nil
		}
	})

	err := f.Execute(w)

	select {
	case <-err:
	case <-time.After(500 * time.Millisecond):
		assert.Fail(t, "Channel should have been closed")
	}
}

func TestJobDispatching(t *testing.T) {
	done := make(chan struct{})

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", nil, task.NewNoOpBroker())

	f := tf.NewDispatcher("foo", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey: task.Property("/foo/bar/mytimeout", 5),
	})

	w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
		return func() error {
			close(done)
			return nil
		}
	})

	f.Execute(w)
	select {
	case <-done:
	case <-time.After(50 * time.Millisecond):
		assert.Fail(t, "Job did not execute")
	}
}

func TestJobDispatchingShouldFallbackOnTimeout(t *testing.T) {
	done := make(chan struct{})

	m := sources.NewMemorySource()
	m.Set("timeout", 10)
	c := config.NewConfigurator(m)

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("foo", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey: task.Property("timeout", 5),
	})

	w := task.NewWork(func(d task.MessageBroker) func() error {
		return func() error {
			time.Sleep(1 * time.Second)
			assert.Fail(t, "Work should be cancelled.")
			return nil
		}
	}, func(m task.MessageBroker) func(error) error {
		return func(e error) error {
			close(done)
			return nil
		}
	})

	f.Execute(w)

	select {
	case <-done:
	case <-time.After(50 * time.Millisecond):
		assert.Fail(t, "Timeout, work should have finished in callback.")
	}
}

func TestJobDispatchingShouldRecoverInFallback(t *testing.T) {
	done := make(chan struct{})

	m := sources.NewMemorySource()
	m.Set("timeout", 50)
	c := config.NewConfigurator(m)

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("foo", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey: task.Property("timeout", 5),
	})

	w := task.NewWork(func(d task.MessageBroker) func() error {
		return func() error {
			return fmt.Errorf("error")
		}
	}, func(m task.MessageBroker) func(error) error {
		return func(e error) error {
			assert.NotNil(t, e)
			close(done)
			return nil
		}
	})

	f.Execute(w)

	select {
	case <-done:
	case <-time.After(150 * time.Millisecond):
		assert.Fail(t, "Timeout, work should have finished in callback.")
	}
}

func TestJobDispatchingShouldTimeout(t *testing.T) {
	done := make(chan struct{})

	m := sources.NewMemorySource()
	m.Set("timeout", 50)
	c := config.NewConfigurator(m)

	tf := task.NewDispatcherFactory(context.NewTaskCtx(), "", c, task.NewNoOpBroker())
	f := tf.NewDispatcher("foo", map[task.DispatchPropertyKey]task.DispatchPropertyValue{
		task.TimeoutKey: task.Property("timeout", 5),
	})
	w := task.NewWorkWithoutFallback(func(d task.MessageBroker) func() error {
		return func() error {
			return fmt.Errorf("error")
		}
	})

	f.Execute(w)

	select {
	case <-done:
		assert.Fail(t, "Job finished execution, should timeout")
	case <-time.After(150 * time.Millisecond):
	}
}
