package event

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCustomEventInReservedRangeShouldFail(t *testing.T) {
	evt, err := NewEventType(min - 10)
	assert.NotNil(t, err)
	assert.Equal(t, Undefined, evt)
}

func TestCustomEventShouldSucceed(t *testing.T) {
	evt, err := NewEventType(min + 10)
	assert.Nil(t, err)
	assert.Equal(t, fmt.Sprintf("%v", min+10), fmt.Sprintf("%v", evt))
}
