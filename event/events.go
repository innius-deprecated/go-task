package event

import "fmt"

type EventType int

const min int = 64
const Undefined EventType = EventType(0)
const Schedule EventType = EventType(1)
const Start EventType = EventType(2)
const StartFallback EventType = EventType(4)
const Error EventType = EventType(8)
const Complete EventType = EventType(16)

func NewEventType(code int) (EventType, error) {
	if code <= min {
		return Undefined, fmt.Errorf("Custom events can only be created with ID's > %v", min)
	}
	return EventType(code), nil
}
